package br.com.uol.pagseguro.testesautomatizados.features.login;

import br.com.uol.pagseguro.testesautomatizados.features.login.LoginPresenter;
import toothpick.config.Module;

public class LoginModule extends Module {
    public LoginModule() {
        bind(LoginPresenter.class).to(LoginPresenter.class);
    }
}
