package br.com.uol.pagseguro.testesautomatizados.models;

public class Credentials {
    private final String userName;
    private final String passWord;

    public Credentials(String userName, String passWord) {
        this.userName = userName;
        this.passWord = passWord;
    }
}
