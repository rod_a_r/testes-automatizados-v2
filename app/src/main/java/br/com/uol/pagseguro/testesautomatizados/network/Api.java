package br.com.uol.pagseguro.testesautomatizados.network;

import br.com.uol.pagseguro.testesautomatizados.models.Credentials;
import br.com.uol.pagseguro.testesautomatizados.models.UserProfile;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface Api {
    @POST("/login")
    Single<UserProfile> login(@Body Credentials credentials);
}
