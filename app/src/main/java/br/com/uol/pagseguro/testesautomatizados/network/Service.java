package br.com.uol.pagseguro.testesautomatizados.network;

import javax.inject.Inject;

import br.com.uol.pagseguro.testesautomatizados.models.Credentials;
import br.com.uol.pagseguro.testesautomatizados.models.UserProfile;
import io.reactivex.Single;

public class Service {

    private Api mApi;

    @Inject
    public Service(Api api) {
        mApi = api;
    }

    public Single<UserProfile> login(Credentials credentials) {
        return mApi.login(credentials);
    }
}
