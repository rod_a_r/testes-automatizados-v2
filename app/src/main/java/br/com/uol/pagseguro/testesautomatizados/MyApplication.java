package br.com.uol.pagseguro.testesautomatizados;

import android.app.Application;
import android.content.Context;

import toothpick.Scope;
import toothpick.Toothpick;

public class MyApplication extends Application {
    private static Context sContext;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = this;
        Scope appScope = Toothpick.openScope(this);
        appScope.installModules(new ApplicationModule());
    }

    public static Context context() {
        return sContext;
    }
}
