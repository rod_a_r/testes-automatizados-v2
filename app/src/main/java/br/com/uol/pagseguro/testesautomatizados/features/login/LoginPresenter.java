package br.com.uol.pagseguro.testesautomatizados.features.login;

import android.support.annotation.NonNull;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.Date;

import javax.inject.Inject;

import br.com.uol.pagseguro.testesautomatizados.exceptions.NoSessionFoundException;
import br.com.uol.pagseguro.testesautomatizados.models.Credentials;
import br.com.uol.pagseguro.testesautomatizados.network.Service;
import br.com.uol.pagseguro.testesautomatizados.storages.SessionStorage;

public class LoginPresenter extends MvpBasePresenter<LoginView> {
    public static final String UNAUTHORIZED = "HTTP 401";
    private Service mService;
    private SessionStorage mStorage;

    @Inject
    public LoginPresenter(Service service, SessionStorage storage) {
        mService = service;
        mStorage = storage;
    }

    @Override
    public void attachView(@NonNull LoginView view) {
        super.attachView(view);
        try {
            view.goToMain(mStorage.load());
        } catch (NoSessionFoundException e) {
            // User is not authenticated
        }
    }

    public void doLogin(String user, String pass) {
        mService.login(new Credentials(user, pass))
                .compose(getView().onThreads())
                .doOnSuccess(mStorage::save)
                .subscribe(
                        profile -> ifViewAttached(view -> view.goToMain(profile)),
                        error -> handleError(error)
                );
    }

    private void handleError(Throwable error) {
        ifViewAttached(view -> {
            if (error.getMessage().contains(UNAUTHORIZED)) {
                view.showError("Usuário ou senha inválidos!");
            }
        });
    }
}
