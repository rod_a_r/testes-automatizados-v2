package br.com.uol.pagseguro.testesautomatizados.storages;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

import br.com.uol.pagseguro.testesautomatizados.exceptions.NoSessionFoundException;
import br.com.uol.pagseguro.testesautomatizados.models.UserProfile;

public class SessionStorage {
    private SharedPreferences preferences;

    @Inject
    public SessionStorage(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public UserProfile load() throws NoSessionFoundException {
        UserProfile profile = new UserProfile();
        profile.name = preferences.getString("UserName", "");
        if ("".equals(profile.name)) {
            throw new NoSessionFoundException();
        }
        Set<String> roles = preferences.getStringSet("Roles", new HashSet<>());
        profile.roles = roles.toArray(new String[roles.size()]);
        return profile;
    }

    public void save(UserProfile profile) {
        preferences.edit()
                .putString("UserName", profile.name)
                .putStringSet("Roles", new HashSet<>(Arrays.asList(profile.roles)))
                .apply();
    }
}
