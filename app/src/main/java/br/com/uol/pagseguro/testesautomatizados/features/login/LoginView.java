package br.com.uol.pagseguro.testesautomatizados.features.login;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import br.com.uol.pagseguro.testesautomatizados.models.UserProfile;
import io.reactivex.SingleTransformer;

public interface LoginView extends MvpView {

    void goToMain(UserProfile profile);

    <T> SingleTransformer<T, T> onThreads();

    void showError(String message);
}
