package br.com.uol.pagseguro.testesautomatizados;

import android.content.Context;

import br.com.uol.pagseguro.testesautomatizados.network.Api;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import toothpick.config.Module;

public class ApplicationModule extends Module {
    ApplicationModule() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://localhost:8080")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        bind(Api.class).toInstance(retrofit.create(Api.class));
        bind(Context.class).toInstance(MyApplication.context());
    }
}
