package br.com.uol.pagseguro.testesautomatizados.features.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.widget.EditText;
import android.widget.Toast;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import javax.inject.Inject;

import br.com.uol.pagseguro.testesautomatizados.R;
import br.com.uol.pagseguro.testesautomatizados.features.home.MainActivity;
import br.com.uol.pagseguro.testesautomatizados.models.UserProfile;
import io.reactivex.SingleTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import toothpick.Scope;
import toothpick.Toothpick;

public class LoginActivity extends MvpActivity<LoginView, LoginPresenter> implements LoginView {
    @Inject
    LoginPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Scope scope = Toothpick.openScopes(getApplication(), this);
        scope.installModules(new LoginModule());
        Toothpick.inject(this, scope);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setUp();
    }

    private void setUp() {
        findViewById(R.id.buttonLogin).setOnClickListener(v -> logIn());
    }

    private void logIn() {
        String userName = ((EditText) findViewById(R.id.editUsername)).getText().toString();
        String passWord = ((EditText) findViewById(R.id.editPassword)).getText().toString();
        mPresenter.doLogin(userName, passWord);
    }

    public void goToMain(UserProfile profile) {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.putExtra("Name", profile.name);
        startActivity(intent);
    }

    @Override
    public <T> SingleTransformer<T, T> onThreads() {
        return upstream -> upstream
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public void showError(String message) {
        Snackbar.make(getWindow().getDecorView(), message, Toast.LENGTH_LONG).show();
    }

    @NonNull
    @Override
    public LoginPresenter createPresenter() {
        return mPresenter;
    }
}
