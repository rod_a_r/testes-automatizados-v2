package br.com.uol.pagseguro.testesautomatizados;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import br.com.uol.pagseguro.testesautomatizados.models.Credentials;
import br.com.uol.pagseguro.testesautomatizados.models.UserProfile;
import br.com.uol.pagseguro.testesautomatizados.network.Api;
import br.com.uol.pagseguro.testesautomatizados.network.Service;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

public class ServiceTest {

    @Rule
    public MockitoRule mRule = MockitoJUnit.rule();

    @Mock
    private Api mApi;

    @Mock
    private UserProfile mProfile;

    @Test
    public void succeededLogin() {
        doReturn(Single.just(mProfile)).when(mApi).login(any(Credentials.class));

        TestObserver<UserProfile> observer = new Service(mApi)
                .login(new Credentials("", ""))
                .test();
        observer.awaitTerminalEvent();
        observer.assertNoErrors();
        observer.assertResult(mProfile);
    }

}
