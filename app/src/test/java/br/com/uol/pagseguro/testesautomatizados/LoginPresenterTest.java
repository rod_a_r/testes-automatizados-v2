package br.com.uol.pagseguro.testesautomatizados;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import br.com.uol.pagseguro.testesautomatizados.exceptions.NoSessionFoundException;
import br.com.uol.pagseguro.testesautomatizados.features.login.LoginPresenter;
import br.com.uol.pagseguro.testesautomatizados.features.login.LoginView;
import br.com.uol.pagseguro.testesautomatizados.models.Credentials;
import br.com.uol.pagseguro.testesautomatizados.models.UserProfile;
import br.com.uol.pagseguro.testesautomatizados.network.Service;
import br.com.uol.pagseguro.testesautomatizados.storages.SessionStorage;
import io.reactivex.Single;
import io.reactivex.SingleTransformer;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class LoginPresenterTest {
    @Rule
    public MockitoRule mRule = MockitoJUnit.rule();

    @Mock
    private Service mService;

    @Mock
    private LoginView mView;

    @Mock
    private SessionStorage mStorage;

    @Mock
    private UserProfile mProfile;

    private LoginPresenter mPresenter;

    @Before
    public void setUp() throws Exception {
        doReturn((SingleTransformer<UserProfile, UserProfile>) upstream -> upstream)
                .when(mView).onThreads();
        doThrow(NoSessionFoundException.class).when(mStorage).load();
        mPresenter = new LoginPresenter(mService, mStorage);
        mPresenter.attachView(mView);
    }

    @Test
    public void doLoginWithSuccess() throws Exception {
        Single<UserProfile> response = Single.just(mProfile);
        doReturn(response).when(mService).login(any(Credentials.class));

        mPresenter.doLogin("user", "pass");

        verify(mView).goToMain(eq(mProfile));
    }

    @Test
    public void takeUserToMainScreenIfHeIsAlreadyAuthenticated() throws Exception {
        doReturn(mProfile).when(mStorage).load();

        mPresenter = new LoginPresenter(mService, mStorage);
        mPresenter.attachView(mView);

        verify(mView).goToMain(eq(mProfile));
    }

    @Test
    public void saveUserProfileAfterSuccessfullyLogin() throws Exception {
        Single<UserProfile> response = Single.just(mProfile);
        doReturn(response).when(mService).login(any(Credentials.class));

        mPresenter.doLogin("user", "pass");

        verify(mStorage).save(eq(mProfile));
    }
}