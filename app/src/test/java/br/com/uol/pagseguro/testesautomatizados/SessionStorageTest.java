package br.com.uol.pagseguro.testesautomatizados;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.util.Arrays;
import java.util.HashSet;

import br.com.uol.pagseguro.testesautomatizados.exceptions.NoSessionFoundException;
import br.com.uol.pagseguro.testesautomatizados.models.UserProfile;
import br.com.uol.pagseguro.testesautomatizados.storages.SessionStorage;

import static com.google.common.truth.Truth.assertThat;

@RunWith(RobolectricTestRunner.class)
public class SessionStorageTest {

    @Rule
    public MockitoRule mRule = MockitoJUnit.rule();

    private Context mContext = RuntimeEnvironment.application;

    private SessionStorage mStorage = new SessionStorage(mContext);


    @Test
    public void retrieveUserSession() throws Exception {
        PreferenceManager.getDefaultSharedPreferences(mContext)
                .edit()
                .putString("UserName", "Rodrigo")
                .putStringSet("Roles", new HashSet<>(Arrays.asList("SELLER", "REPORT")))
                .apply();

        UserProfile profile = mStorage.load();

        assertThat(profile.name).isEqualTo("Rodrigo");
        assertThat(profile.roles).asList().containsExactly("SELLER", "REPORT");
    }

    @Test(expected = NoSessionFoundException.class)
    public void noSessionFound() throws Exception {
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().clear().apply();
        mStorage.load();
    }

    @Test
    public void saveUserSession() throws Exception {
        UserProfile profile = new UserProfile();
        profile.name = "Rodrigo";
        profile.roles = new String[]{"ADMIN"};

        mStorage.save(profile);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);

        assertThat(preferences.getString("UserName","")).isEqualTo("Rodrigo");
        assertThat(profile.roles).asList().containsExactly("ADMIN");
    }
}
