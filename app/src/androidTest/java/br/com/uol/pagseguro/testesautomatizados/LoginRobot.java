package br.com.uol.pagseguro.testesautomatizados;

import android.preference.PreferenceManager;

import com.squareup.rx2.idler.Rx2Idler;

import org.bigtesting.fixd.Method;
import org.bigtesting.fixd.ServerFixture;

import java.io.IOException;

import br.com.uol.cappuccino.TestRobot;
import br.com.uol.pagseguro.testesautomatizados.features.login.LoginActivity;
import io.reactivex.plugins.RxJavaPlugins;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

class LoginRobot extends TestRobot<LoginActivity> {

    private ServerFixture mServer = new ServerFixture(8080);

    LoginRobot() {
        super(LoginActivity.class);
        RxJavaPlugins.setInitNewThreadSchedulerHandler(Rx2Idler.create("RxJava 2.x NewThread Scheduler"));
    }

    LoginRobot setUpServerWithSuccess() {
        mServer.handle(Method.POST, "/login", "application/json")
                .with(200, "application/json",
                        "{\"name\": \"Rodrigo\",\"roles\": [\"ADMIN\"]}");
        return this;
    }

    LoginRobot setUserIsUnauthorized() {
        mServer.handle(Method.POST, "/login", "application/json")
                .with(401, "application/json",
                        "{\"message\":\"Usuario ou senha inválidos!\"}");
        return this;
    }

    @Override
    public <TR extends TestRobot<LoginActivity>> TR start() {
        try {
            mServer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return super.start();
    }

    LoginRobot loginWith(String userName, String password) {
        onView(withContentDescription("UserName")).perform(typeText(userName));
        onView(withContentDescription("Password")).perform(typeText(password));
        capture("TypingCredentials");
        onView(withContentDescription("Login")).perform(click());
        return this;
    }

    LoginRobot setUserIsAuthenticated() {
        PreferenceManager.getDefaultSharedPreferences(MyApplication.context())
                .edit().putString("UserName", "Rodrigo").apply();
        return this;
    }

    LoginRobot setNoUserAuthenticated() {
        PreferenceManager.getDefaultSharedPreferences(MyApplication.context())
                .edit().clear().apply();
        return this;
    }

    void checkWelcomeMessageFor(final String name) {
        onView(withText("Bem vindo " + name + "!")).check(matches(isDisplayed()));
        capture("UserIsAuthenticated");
    }

    void checkErrorMessage(String errorMessage) {
        onView(withText(errorMessage)).check(matches(isDisplayed()));
        capture("UserIsUnauthorized");
    }


    void close() throws IOException {
        mServer.stop();
    }
}
