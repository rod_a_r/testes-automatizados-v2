package br.com.uol.pagseguro.testesautomatizados;

import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import br.com.uol.cappuccino.EspressoTest;
import br.com.uol.pagseguro.testesautomatizados.features.home.MainActivity;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class AcceptationTest {

    @Rule
    public LoginRobot mRobot = new LoginRobot();

    @After
    public void afterDestroy() {
        try {
            mRobot.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void logInSuccessfully() throws IOException {
        mRobot.setUpServerWithSuccess()
                .setNoUserAuthenticated()
                .<LoginRobot>start()
                .loginWith("ramaro", "123teste")
                .checkWelcomeMessageFor("Rodrigo");
    }

    @Test
    public void userIsAlreadyAuthenticated() throws IOException {
        mRobot.setUserIsAuthenticated()
                .<LoginRobot>start()
                .checkWelcomeMessageFor("Rodrigo");
    }

    @Test
    public void userCredentialsAreIncorrect() throws IOException {
        mRobot.setUserIsUnauthorized()
                .setNoUserAuthenticated()
                .<LoginRobot>start()
                .loginWith("ramaro", "123teste")
                .checkErrorMessage("Usuário ou senha inválidos!");
    }

}
